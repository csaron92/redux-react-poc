import {NEW_POST, FETCH_POSTS } from '../actions/types';

const initalState = {
    items: [], //all posts
    item: {} //single post
}

export default function(state = initalState, action){
    switch(action.type){
        case FETCH_POSTS:
            return {
                ...state,
                items: action.payload
            };
        case NEW_POST:
            return {
                ...state,
                item: action.payload
            };
        break;
        default:
            return state;
    }
}